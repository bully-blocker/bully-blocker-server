from flask import Flask, Response, render_template, redirect, session, request, jsonify, send_file
from flask_session import Session
from flask_cors import CORS
import tweepy
from helper import *
import os
import redis
import datetime
import json
import facebook

app = Flask(__name__)
app.secret_key = os.urandom(24)
CORS(app, resources={r'/*': {'origins': 'http://bully-blocker.herokuapp.com'}})

# Setup files
with open("creds.json", "w+") as f:
    f.write(os.environ['CREDS'])
    f.close()

with open("pyrebase.json", "w+") as f:
    f.write(os.environ['PYREBASE'])
    f.close()

cred = credentials.Certificate("creds.json")
firebase = firebase_admin.initialize_app(cred, name="bully-blocker")

# Fetching env vars
consumer_key = os.environ['TWITTER_KEY']
consumer_secret = os.environ['TWITTER_SECRET']
port = int(os.environ.get('PORT', 5000))
redis_password = os.environ.get('REDIS_PASSWORD')
azure_key = os.environ.get('AZURE_KEY')
hive_key = os.environ.get('HIVE_KEY')

# Setting up Redis session:
SESSION_REDIS = redis.StrictRedis(host='redis-10468.c1.us-east1-2.gce.cloud.redislabs.com', port=10468, password=redis_password)
SESSION_TYPE = 'redis'
app.secret_key = "asfa786esdnccs9ehskentmcs"
app.config.from_object(__name__)
Session(app)

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kws):
        if not 'user' in session or session['user'] is None:
            return redirect("/sign-in")
        return f(*args, **kws)
    return decorated_function

@app.route("/humans.txt")
def humans():
    return send_file("./static/humans.txt")

@app.route("/moderate", methods=["GET"])
def moderate_tweet():
    text = request.args.get("text")
    # TODO: Replace 0.7 with user threshold or other meaningful value.
    result = moderate(text, azure_key, 0.7)
    return result, 200

# Pages that don't require users to have account:
@app.route("/sign-in", methods=["POST"])
def sign_in():
    email = request.form['email']
    password = request.form['password']
    result = sign_in_user(email, password)
    user = result["user"]
    if user is not None:
        session['user'] = user
        response = {
            "code": 0,
            "user": user,
            "err": result["err"]
        }
    else:
        response = {
            "code": 1,
            "user": user,
            "err": result["err"]
        }
    return jsonify(response)

@app.route("/sign-up", methods=["POST"])
def sign_up():
    email = request.form['email']
    password = request.form['password']
    confirm = request.form['password-confirm']
    err = new_user(firebase, email, password)
    if err is not "":
        response = {
            "user": None,
            "err": err,
            "code": 1
        }
    else:
        user = sign_in_user(email, password)
        session['user'] = user
        response = {
            "user": user,
            "err": err,
            "code": 0
        }
    return jsonify(response)

@app.route("/logout")
def logout():
    if 'user' in session and session['user'] is not None:
        session['user'] = None
    response = {
        "code": 0
    }
    return response

# Logged in Users:

@app.route("/twitter-auth")
@login_required
def twitter_auth():
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    code = 0

    try:
        redirect_url = auth.get_authorization_url()
    except tweepy.TweepError:
        code = 1
        redirect_url = ""

    session['request_token'] = auth.request_token

    response = {
        "request_token": auth.request_token,
        "redirect_url": redirect_url,
        "code": code
    }
    return redirect(redirect_url, code=302)

@app.route("/twitter-callback")
@login_required
def twitter_callback():
    if not 'request_token' in session:
        return redirect('/twitter-auth')

    verifier = request.args.get('oauth_verifier')
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    token = session.get('request_token')
    session.pop('request_token')
    auth.request_token = token

    try:
        auth.get_access_token(verifier)
    except tweepy.TweepError:
        return "error"

    session['access_token'] = auth.access_token
    session['access_secret'] = auth.access_token_secret
    return redirect("/loading-feed")

@app.route("/twitter-feed")
@login_required
def feed():
    if not 'access_token' in session or not 'access_secret' in session:
        return redirect('/twitter-auth')
    key = session['access_token']
    secret = session['access_secret']

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(key, secret)

    feed = twitter_feed(auth)
    tweets = []
    bodies = []

    for tweet in feed:
        pics = twitter_pictures(tweet)
        date = tweet.created_at.strftime('%A, %b %Y')
        username = tweet.user.name
        profile_pic = tweet.user.profile_image_url
        link = "https://twitter.com/statuses/" + tweet.id_str
        body = tweet.text
        bodies.append(body)

        # rating = rate(body, n_words, p_words)

        if len(pics) > 0:
            is_video = "video" in list(pics)[0]
        else:
            is_video = False

        block = False

        if float(rating) > 0:
            overall = "pos"
        else:
            overall = "neg"

        tweets.append({
            "pics": pics,
            "date": date,
            "username": username,
            "profile_pic": profile_pic,
            "body": body,
            "overall": overall,
            "link": link,
            "is_video": is_video,
            "block": block
        })
    batch = []
    batch_size = 3
    for i in range(len(bodies)):
        batch.append(bodies[i])
        if i % batch_size is batch_size - 1:
            batch_size = len(batch)
            print("Batch Size: {0}".format(batch_size))
            # TODO: Replace 0.6 with user threshold.
            result = batch_moderate(batch, azure_key, 0.6)
            if result["multiple"]:
                for j in range(batch_size):
                    index = i-((batch_size-1)-j)
                    tweets[index]["moderation"] = result["result"][j]
                    tweets[index]["moderation"]["percent"] = result["result"][j]["offensive"] * 100
                    offensive = result["result"][j]["offensive"]
                    if offensive < 0.15:
                        color = "#5cb85c"
                    elif offensive < 0.5:
                        color = "#ecc52c"
                    else:
                        color = "#d9534e"
                    # Replace with thresh
                    tweets[index]["block"] = offensive > 0.6
                    tweets[index]["moderation"]["color"] = color
            else:
                for j in range(batch_size):
                    index = i-((batch_size-1)-j)
                    tweets[index]["moderation"] = result["original"]
                    tweets[index]["moderation"]["rating"] = "not offensive in any way."
                    tweets[index]["moderation"]["percent"] = result["original"]["offensive"] * 100
                    offensive = result["original"]["offensive"]
                    if offensive < 0.15:
                        color = "#5cb85c"
                    elif offensive < 0.5:
                        color = "#ecc52c"
                    else:
                        color = "#d9534e"
                    # Replace with thresh
                    tweets[index]["block"] = offensive > 0.6
                    tweets[index]["moderation"]["color"] = color
            batch = []

    if not len(batch) is 0:
        batch_size = len(batch)
        print("Batch Size: {0}".format(batch_size))
        result = batch_moderate(batch, azure_key, 0.6)
        if result["multiple"]:
            for j in range(batch_size):
                index = i-((batch_size-1)-j)
                tweets[index]["moderation"] = result["result"][j]
                tweets[index]["moderation"]["percent"] = result["result"][j]["offensive"] * 100
                offensive = result["result"][j]["offensive"]
                if offensive < 0.33:
                    color = "#5cb85c"
                elif offensive < 0.66:
                    color = "#ecc52c"
                else:
                    color = "#d9534e"
                # Replace with thresh
                tweets[index]["block"] = offensive > 0.6
                tweets[index]["moderation"]["color"] = color
        else:
            for j in range(batch_size):
                index = i-((batch_size-1)-j)
                tweets[index]["moderation"] = result["original"]
                tweets[index]["moderation"]["rating"] = "not offensive in any way."
                tweets[index]["moderation"]["percent"] = result["original"]["offensive"] * 100
                offensive = result["original"]["offensive"]
                if offensive < 0.33:
                    color = "#5cb85c"
                elif offensive < 0.66:
                    color = "#ecc52c"
                else:
                    color = "#d9534e"
                # Replace with thresh
                tweets[index]["block"] = offensive > 0.6
                tweets[index]["moderation"]["color"] = color
    response = {
        "tweets": tweets,
        "code": 0
    }
    return jsonify(response)

@app.route("/twitter-post", methods=["POST"])
@login_required
def post():
    if not 'access_token' in session or not 'access_secret' in session:
        response = {
            "code": 1,
            "err": "not signed in"
        }
        return response

    else:
        body = request.form['body']

        key = session['access_token']
        secret = session['access_secret']
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(key, secret)

        result = post_twitter(auth, body)
        response = {
            "err": result,
            "code": 0
        }
        if result is not "":
            response["code"] = 1

        return response


@app.route("/settings")
@login_required
def settings():
    response = {
        "settings": None,
        "code": 0
    }
    return jsonify(response)

# Tests
@app.route("/generate-password")
def gen_pword():
    response = {
        "password": generate_password(),
        "code": 0
    }
    return jsonify(response)

@app.route("/feed-test")
def feed_test():
    feed = json.loads(open("feed.txt", "r").read())
    tweets = []
    bodies = []

    for tweet in feed:
        tweets.append({
            "pics": tweet["pics"],
            "date": tweet["date"],
            "username": tweet["username"],
            "profile_pic": tweet["profile_pic"],
            "body": tweet["body"],
            "rating": tweet["rating"],
            "link": tweet["link"],
            "moderation": tweet["moderation"],
            "is_video": tweet["is_video"],
            "block": tweet["block"]
        })

    response = {
        "tweets": tweets,
        "code": 0
    }
    return jsonify(response)


app.run(host="0.0.0.0", port=port, debug=True)
